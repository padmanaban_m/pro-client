﻿(function () {
    'use strict';

    angular
        .module('myApp')
        .factory('UserService', UserService);

    UserService.$inject = ['$http', '$window', 'API'];
    function UserService($http, $window, API) {
        var service = {};
        service.AuthenticateUser = AuthenticateUser;
        service.CreateUser = CreateUser;
        service.SetCredentials = SetCredentials;
        service.isAuthenticated = isAuthenticated;
        service.getUserObject = getUserObject;
        return service;

        function AuthenticateUser(user) {
            return $http({
                method: 'POST',
                url: API+"/clientLogin",
                data: user,
                headers: { 'Content-Type': 'application/json' },
            }).then(handleSuccess, handleError);
        }

        function CreateUser(user) {
            return $http({
                method: 'POST',
                url: API+"/clientSignup/",
                data: user,
                headers: { 'Content-Type': 'application/json' },
            }).then(handleSuccess, handleError);

        }

        function SetCredentials(token) {
            $window.localStorage.setItem('jwt', token);
        }

        function isAuthenticated(){
            
            if($window.localStorage.getItem('jwt')){
                return true;
            }else{
                return false;
            }
        }

        function getUserObject(){
            
            if($window.localStorage.getItem('jwt')){
                let token = $window.localStorage.getItem('jwt');
                let userPart = token.split('.')[1];
                return userPart && atob(userPart);
            }
        }

        // private functions

        function handleSuccess(result) {
            return { success: true, result: result.data };
        }

        function handleError(error) {
            return { success: false, message: error.data && error.data.err || 'Error Occured' };
        }
    }

})();
