﻿(function () {
    'use strict';

    angular
        .module('myApp')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$location', 'UserService', 'FlashService'];
    function LoginController($location, UserService, FlashService) {
        var vm = this;

        vm.login = login;

        (function initController() {
        })();

        function login() {
            vm.dataLoading = true;

            UserService.AuthenticateUser(vm.user).then(function (response) {
                if (response.success) {
                    FlashService.Success('Log In successful', true);                                    
                    UserService.SetCredentials(response.result.token);
                    $location.path('/team');
                    vm.dataLoading = false;
                } else {
                    FlashService.Error(response.message);
                    vm.dataLoading = false;
                }
            });
        };
    }

})();
