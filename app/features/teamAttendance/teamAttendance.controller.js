(function () {
    'use strict';

    angular.module('myApp')
        .controller('TeamAttendanceController', TeamAttendanceController);

    TeamAttendanceController.$inject = ['$routeParams'];
    function TeamAttendanceController($routeParams) {
        var vm = this;
        (function initController() {
            vm.teamObj = $routeParams.teamObj;
        })();
    }

})();