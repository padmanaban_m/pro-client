(function () {
    'use strict';

    angular.module('myApp')
        .controller('TeamController', TeamController);

    TeamController.$inject = ['$http', 'API', '$location'];
    function TeamController($http, API, $location) {
        var vm = this;
        vm.teams = [];
        vm.onTileClick = onTileClick;
        (function initController() {
            return $http({
                method: 'GET',
                url: API + "/Team/"
            }).then(function (team) { vm.teams = team.data; }, function () { console.log('Error getting Team') });
        })();

        function onTileClick(team) {
            $location.path('/teamAttendance').search({ 'teamObj': team });
        }
    }

})();