(function () {

    'use strict';

    angular.module('myApp')
        .controller('Main', MainCtrl);

    MainCtrl.$inject = ['$window', '$scope', '$location', 'UserService', '$http', 'API', 'FlashService'];
    // We won't touch anything in here
    function MainCtrl($window, $scope, $location, UserService, $http, API, FlashService) {

        $scope.logout = function () {
            let User = $scope.getUserObject();

            return $http({
                method: 'POST',
                url: API + "/clientLogout/",
                data: User,
                headers: { 'Content-Type': 'application/json' },
            }).then(function () {
                FlashService.Success('Log Out successful', true);                
                $window.localStorage.removeItem('jwt');
                $location.path('/login');
            }, function () {

            });
        }

        $scope.isAuthenticated = function () {
            return UserService.isAuthenticated();
        }
        $scope.getUserObject = function (key) {
            if ($window.localStorage.getItem('jwt')) {
                let token = $window.localStorage.getItem('jwt');
                let userPart = token.split('.')[1];
                if (key) {
                    return userPart && JSON.parse(atob(userPart))[key].toUpperCase();
                } else {
                    return userPart && JSON.parse(atob(userPart));
                }

            }
        }
    }

})();