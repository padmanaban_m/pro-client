(function () {
    'use strict';

    angular.module('myApp')
        .run(redirectionHandler);

    redirectionHandler.$inject = ["$state", "$rootScope", 'UserService', '$location']
    function redirectionHandler($state, $rootScope, UserService, $location) {
        $rootScope.$on('$routeChangeStart',
            function (event, toState, toParams, fromState, fromParams) {
                if ((toState.$$route && toState.$$route.authenticate) && !UserService.isAuthenticated()) {
                    $location.path('#!/login');
                }
                if (toState.$$route && toState.$$route.originalPath == '/login' && UserService.isAuthenticated()) {
                    $location.path('/');
                }
            })
    }

})()