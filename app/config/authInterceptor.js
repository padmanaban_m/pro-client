(function () {

    'use strict';
    angular.module('myApp')
        .factory('authInterceptor', authInterceptor);

    authInterceptor.$inject = ['$window', '$q'];

    function authInterceptor($window, $q) {
        return {
            // automatically attach Authorization header
            request: function (config) {
                var token = $window.localStorage.getItem('jwt');
                if (token && !config.headers.hasOwnProperty('Authorization')) {
                    config.headers['Authorization'] = 'Bearer ' + token;
                }
                return config;
            },

            // If a token was sent back, save it
            response: function (res) {
                return res;
            },
        }
    }

})();