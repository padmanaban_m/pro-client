(function () {

    'use strict';

    angular.module('myApp')
        // .constant('API', 'http://localhost:1337');
    .constant('API', 'https://pro-services.herokuapp.com');   

})();