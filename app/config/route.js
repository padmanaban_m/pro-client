(function () {
    'use strict';

    angular.module('myApp')
        .config(config);

    config.$inject = ['$routeProvider', '$httpProvider'];
    function config($routeProvider, $httpProvider) {
        $routeProvider
            .when('/', {
                controller: 'HomeController',
                templateUrl: 'features/home/home.view.html',
                controllerAs: 'vm'
            })
            .when('/login', {
                controller: 'LoginController',
                templateUrl: 'features/login/login.view.html',
                controllerAs: 'vm'
            })
            .when('/register', {
                controller: 'RegisterController',
                templateUrl: 'features/register/register.view.html',
                controllerAs: 'vm'
            })
            .when('/team', {
                controller: 'TeamController',
                templateUrl: 'features/team/team.view.html',
                controllerAs: 'vm',
                authenticate: true
            })
            .when('/teamAttendance', {
                controller: 'TeamAttendanceController',
                templateUrl: 'features/teamAttendance/teamAttendance.view.html',
                controllerAs: 'vm',
                authenticate: true

            })
            .otherwise({ redirectTo: '/login' });

        $httpProvider.interceptors.push('authInterceptor');

    }

})();